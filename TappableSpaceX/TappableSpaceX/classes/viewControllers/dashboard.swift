//
//  dashboard.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

extension UILabel
{
    convenience init(labelText:String, bold:Bool = false)
    {
        self.init()
        if bold
        {
            self.font = UIFont.boldSystemFont(ofSize: 18)
        }
        else
        {
            self.font = UIFont.systemFont(ofSize: 16)
        }
        self.backgroundColor = .clear
        self.textColor = .midnightBlue
        self.text = labelText
        self.numberOfLines = 0
        self.adjustsFontSizeToFitWidth = true
        self.frame.size.width = UIScreen.main.bounds.width - (dashboard.padding * 2)
        self.sizeToFit()
    }
}
extension UIColor
{
    convenience init(red:Int, green:Int, blue:Int)
    {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1)
    }
    //colours from https://flatuicolors.com/palette/defo
    static let emerald = UIColor(red: 46, green: 204, blue: 113) //green
    static let carrot = UIColor(red: 230, green: 126, blue: 34) //orange
    static let alizarin = UIColor(red: 231, green: 76, blue: 60) //red
    static let midnightBlue = UIColor(red: 44, green: 62, blue: 80)
    static let clouds = UIColor(red: 236, green: 240, blue: 241) //off white
    static let asbestos = UIColor(red: 127, green: 140, blue: 141)
    static let amethyst = UIColor(red: 155, green: 89, blue: 182)
}

class dashboard:UIViewController, UITableViewDelegate, UITableViewDataSource
{
    static let padding:CGFloat = 16
    
    var pTableView: UITableView = UITableView(frame: UIScreen.main.bounds)
    var tableObjects:[UITableViewCell] = []
    
    let padding: CGFloat = 16.0
    
    var pRest:restAPI
    
    let pActivity = UIActivityIndicatorView(style: .large)

    init()
    {
        pRest = restAPI(urlString: "https://api.spacexdata.com/v3/rockets")
        
        super.init(nibName: nil, bundle: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.redraw), name: restAPI.notificationRocketsDownloaded, object: nil)

        setTableView()
        
        self.view = pTableView
        
        pActivity.center = CGPoint(x: pTableView.frame.size.width / 2, y: pTableView.frame.size.height / 2)
        pActivity.isAccessibilityElement = false
        pActivity.startAnimating()
        pTableView.addSubview(pActivity)
    }
    
    func setTableView()
    {
        pTableView.backgroundColor = .clouds
        pTableView.isOpaque = true

        pTableView.delegate = self
        pTableView.dataSource = self
        
        pTableView.sectionFooterHeight = 0
        pTableView.separatorStyle = .singleLine
        
        pTableView.tableFooterView = UIView()
        
        pTableView.autoresizesSubviews = false
    }
    
    @objc func redraw()
    {
        pActivity.stopAnimating()
        pActivity.removeFromSuperview()
        
        tableObjects.removeAll()

        let spaceX = headerCell(title: "SpaceX rockets")
        tableObjects.append(spaceX)

        var rocketList: [rocketCell] = []
        
        if let rockets = pRest.rocketList
        {
            for rocket in rockets
            {
                let view = rocketBaseView(rocketInfo: rocket, picture: pRest.imageArray[rocketList.count]?.image)
                let cell = rocketCell(view: view)
                rocketList.append(cell)
            }
        }

        if rocketList.count > 0
        {
            for rocket in rocketList
            {
                tableObjects.append(rocket)
            }
        }

        pTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let list = pRest.rocketList
        {
            let index = indexPath.row - 1 //don't include header cell
            let detail = detailedView(rocketInfo: list[index], image: pRest.imageArray[index]?.image)
            detail.modalPresentationStyle = .popover
            
            self.present(detail, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return tableObjects[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return tableObjects[indexPath.row].frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return tableObjects[indexPath.row].frame.size.height
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask
    {
        return [.portrait, .portraitUpsideDown]
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
}
