//
//  detailedView.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

class detailedView: UIViewController
{
    private var pos = CGPoint.zero
    
    private var pView:UIView = UIView(frame: UIScreen.main.bounds)
    
    private var wikiLink:URL?
    
    private let pScrollView = UIScrollView(frame: UIScreen.main.bounds)
    
    init(rocketInfo: Dictionary<String, Any>, image: UIImage?)
    {
        super.init(nibName: nil, bundle: nil)

        self.view = pScrollView
        
        self.view.backgroundColor = .clouds
        
        let view = rocketBaseView(rocketInfo: rocketInfo, picture: image)
        self.view.addSubview(view)
        
        addDetailView(rocketInfo: rocketInfo, atY: view.frame.size.height)
    }
    
    private func addDetailView(rocketInfo: Dictionary<String, Any>, atY:CGFloat)
    {
        pView.backgroundColor = .clouds
        pView.frame.origin.y = atY
        
        pos = CGPoint(x: dashboard.padding, y: 0)
        
        if let active = rocketInfo["active"] as? Bool
        {
            addDetail(text: active ? "Active" : "Inactive")
        }
        
        if let country = rocketInfo["country"] as? String
        {
            addDetail(text: "Country: \(country)")
        }

        if let description = rocketInfo["description"] as? String
        {
            addDetail(text: description)
        }

        if let cost = rocketInfo["cost_per_launch"] as? NSNumber
        {
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.usesGroupingSeparator = true
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 0
            
            if let costString = formatter.string(from: cost)
            {
                addDetail(text: "Cost per launch: \(costString)")
            }
        }

        if let wiki = rocketInfo["wikipedia"] as? String
        {
            self.wikiLink = URL(string: wiki)
            
            if wikiLink != nil
            {
                let button = UIButton(type: .custom)
                button.backgroundColor = .amethyst
                button.setTitle("View on Wikipedia", for: .normal)
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16)
                button.setTitleColor(.clouds, for: .normal)
                button.frame.origin = pos
                button.frame.size = CGSize(width: UIScreen.main.bounds.width - (dashboard.padding * 2), height: button.titleLabel!.frame.size.height + (dashboard.padding * 2))
                button.layer.cornerRadius = dashboard.padding // button.frame.size.height / 2
                
                button.addTarget(self, action: #selector(openWiki), for: .touchUpInside)
                
                pView.addSubview(button)
                
                pos.y += button.frame.size.height + dashboard.padding
            }
        }

        pView.frame.size.height = pos.y
        self.view.addSubview(pView)
        
        self.pScrollView.contentSize.height = atY + pos.y + (dashboard.padding * 2)
    }
    
    @objc private func openWiki()
    {
        if let link = wikiLink
        {
            UIApplication.shared.open(link, options: [:], completionHandler: nil)
        }
    }
    
    private func addDetail(text:String)
    {
        let label = UILabel(labelText: text)
        label.frame.origin = pos
        pView.addSubview(label)
        pos.y += label.frame.size.height + dashboard.padding
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
