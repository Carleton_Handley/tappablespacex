//
//  headerCell.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

class headerCell: UITableViewCell
{
    init(title: String)
    {
        super.init(style: .default, reuseIdentifier: nil)
        
        self.selectionStyle = .none
        
        textLabel?.text = title
        
        self.backgroundColor = .alizarin
        textLabel?.textColor = .clouds
        
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
