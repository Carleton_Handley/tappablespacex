//
//  rocketBaseView.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

class rocketBaseView:UIView
{
    init(rocketInfo: Dictionary<String, Any>, picture: UIImage?)
    {
        super.init(frame: UIScreen.main.bounds)
        
        var pos:CGPoint = CGPoint(x: dashboard.padding, y: 0)
        
        if let image = picture
        {
            let width = UIScreen.main.bounds.width // - (dashboard.padding * 2)
            let aspect = width / image.size.width
            let height = image.size.height * aspect
            let picView = UIImageView(frame: CGRect(x: 0, y: pos.y, width: width, height: height))
            picView.image = image
            self.addSubview(picView)
            pos.y += picView.frame.size.height
        }
        
        pos.y += dashboard.padding

        let nameText:String
        if let name = rocketInfo["rocket_name"] as? String
        {
            nameText = "Name: \(name)"
        }
        else
        {
            nameText = "Name: Unknown"
        }
            
        let nameLabel = UILabel(labelText: nameText, bold: true)
        nameLabel.frame.origin = pos
        self.addSubview(nameLabel)
        pos.y += nameLabel.frame.size.height + dashboard.padding

        if let successRate = rocketInfo["success_rate_pct"] as? Int
        {
            let imageName:String
            let imageColour:UIColor
            
            if successRate >= 60
            {
                imageName = "smiling"
                imageColour = .emerald
            }
            else if successRate >= 30
            {
                imageName = "neutral"
                imageColour = .carrot
            }
            else
            {
                imageName = "sad"
                imageColour = .alizarin
            }
            
            if let image = UIImage(named: imageName)?.withTintColor(imageColour)
            {
                let successLabel = UILabel(labelText: "Success rate: \(successRate)%")
                successLabel.frame.origin = pos
                self.addSubview(successLabel)
                pos.y += successLabel.frame.size.height + (dashboard.padding / 2)

                let imageView = UIImageView(frame:CGRect(origin: pos, size: CGSize(width: 32, height: 32)))
                imageView.image = image
                self.addSubview(imageView)
                pos.y += imageView.frame.size.height + dashboard.padding
            }
        }
        
        if let firstFlightDate = rocketInfo["first_flight"] as? String
        {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "yyyy-MM-dd"
            
            if let theDate = df.date(from: firstFlightDate)
            {
                let nf = NumberFormatter()
                nf.numberStyle = .ordinal
                
                let calendar = Calendar.current.dateComponents([.day], from: theDate)
                let num = NSNumber(integerLiteral: calendar.day!)
                if let dayOrdinal = nf.string(from: num)
                {
                    df.dateFormat = "MMMM YYYY"

                    let dateString = df.string(from: theDate)
                    
                    let dateLabel = UILabel(labelText: "First launch: \(dayOrdinal) \(dateString)")
                    self.addSubview(dateLabel)
                    dateLabel.frame.origin = pos
                    pos.y += dateLabel.frame.size.height + dashboard.padding
                }
            }

        }
        self.frame.size.height = pos.y
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
}
