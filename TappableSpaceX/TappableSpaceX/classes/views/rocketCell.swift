//
//  rocketCell.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

class rocketCell: UITableViewCell
{
    init(view:UIView)
    {
        super.init(style: .default, reuseIdentifier: nil)
        
        self.selectionStyle = .none
        
        self.textLabel?.isHidden = true
        
        self.addSubview(view)
        self.frame.size.height = view.frame.size.height
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
