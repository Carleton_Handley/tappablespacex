//
//  server.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation

class server : NSObject
{
    let serverLogging = true
    
    enum downloadState:Int
    {
        case idle
        case downloading
        case complete
        case error
    }
    
    var validData = false
    
    var downloadingState = downloadState.idle
    
    var json:NSDictionary? = nil
    
    var urlString:String! = nil

    init(urlString:String)
    {
        self.urlString = urlString
        super.init()
        
        self.download()
    }
    
    func download()
    {
        if let url = URL(string: self.urlString)
        {
            localLog("Request: \(url.absoluteString)")
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            var req = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval:30)
            req.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            req.httpMethod = "GET"

            downloadingState = .downloading
            
            let task = session.dataTask(with: req)
            { (data, response, error) -> Void in
                
                if error != nil
                {
                    self.localLog("Error with request\n" + error!.localizedDescription)
                    self.downloadingState = .error
                    self.serverComplete()
                }
                else if data != nil && data!.count > 0
                {
                    self.downloadingState = .complete
                    
                    DispatchQueue.main.async
                    {
                        self.scrapeData(data: data!)
                        self.serverComplete()
                    }
                }
                else
                {
                    self.localLog("No data from \(response!.url!)")
                    self.localLog(response!.debugDescription)
                    self.downloadingState = .error
                    self.serverComplete()
                }
            }
            task.resume()
        }
        else
        {
            downloadingState = .error
        }
    }
    
    func serverComplete()
    {
    }
    
    func scrapeData(data:Data)
    {
    }
    
    func localLog(_ text:String)
    {
        #if DEBUG
        if serverLogging
        {
            print(text)
        }
        #endif
    }
}
