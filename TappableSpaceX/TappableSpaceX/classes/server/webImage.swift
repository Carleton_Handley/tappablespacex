//
//  webImage.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

class webImage:server
{
    static let notificationImageDownloaded = Notification.Name("imageDownloaded")
    
    var image:UIImage? = nil
    
    override func scrapeData(data: Data)
    {
        image = UIImage(data: data)
        validData = image != nil
    }
    
    override func serverComplete()
    {
        NotificationCenter.default.post(name: webImage.notificationImageDownloaded, object: nil)
    }
}
