//
//  restAPI.swift
//  TappableSpaceX
//
//  Created by Carleton Handley on 21/01/2021.
//

import Foundation
import UIKit

class restAPI : server
{
    static let notificationRocketsDownloaded = Notification.Name("rocketsDownloaded")

    var rocketList: [Dictionary<String, Any>]? = nil
    var imageArray:[webImage?] = []
    var imageCount = 0
    
    override init(urlString: String)
    {
        super.init(urlString: urlString)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkImages), name: webImage.notificationImageDownloaded, object: nil)
    }
    
    @objc func checkImages()
    {
        imageCount -= 1
        
        if imageCount == 0
        {
            NotificationCenter.default.post(name: restAPI.notificationRocketsDownloaded, object: nil)
        }
    }
    
    override func scrapeData(data: Data)
    {
        let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [Dictionary<String, Any>]
        
        if let rockets = json
        {
            rocketList = rockets
            
            downloadImages()
        }
    }
    
    private func downloadImages()
    {
        if let rockets = self.rocketList
        {
            for rocket in rockets
            {
                if let imageURLS = rocket["flickr_images"] as? [String]
                {
                    if let url = imageURLS.first
                    {
                        let imageDownloader = webImage(urlString: url)
                        imageArray.append(imageDownloader)
                        imageCount += 1
                    }
                }
                else
                {
                    imageArray.append(nil)
                }
            }
        }
    }
}
